#!/usr/bin/python

import sys
import re

class TokenizerException(Exception):

	def __init__(self, tokenizer):
		#self.__tokenizer = tokenizer
		self.__lineno = tokenizer.lineno()
		self.__line = tokenizer.line()

	def lineno(self):
		return self.__lineno

	def line(self):
		return self.__line

	def __str__(self):
		return "invalid token " + str([self.__line]) + " at line " + str(self.__lineno)

class Tokenizer:

	def __init__(self, dbh, errorHandler):
		self.__dbh = dbh
		self.__lineno = 0
		self.__nextlineno = 0
		self.__line = None
		self.__next = None
		self.__re = self.__buildTokenizer()
		self.__errorHandler = errorHandler

		self.__nextLine()
	
	def __buildTokenizer(self):
		def format(tokens):
			for i in range(len(tokens)):
				tokens[i] = re.compile(tokens[i])
			return tokens

		##################################################################
		# == PALAVRAS ==
		#
		# a-z = \u0061-\u007A
		# 
		# a_grave = u"\u00E0"
		# a_acute = u"\u00E1"
		# a_circumflex = u"\u00E2"
		# a_tilde = u"\u00E3"
		#
		# ae = u"\u00E6"
		# c_cedilla = u"\u00E7"
		# e_grave = u"\u00E8"
		# e_acute = u"\u00E9"
		# e_cirfumflex = u"\u00EA"
		#
		# i_grave = u"\u00EC"
		# i_accute = u"\u00ED"
		# i_circumflex = u"\u00EE"
		#		
		# o_grave = u"\u00F2"
		# o_accute = u"\u00F3"
		# o_circumflex = u"\u00F4"
		# o_tilde = u"\u00F5"
		#
		# u_grave = u"\u00F9"
		# u_accute = u"\u00FA"
		# u_circumflex = u"\u00FB"
		word = u"[\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB]+"

		##################################################################
		# == COMENTARIOS ==
		#
		# Os comentarios comecam com # e vao ate ao fim da linha
		comments = "^(#)(.*?)$"

		##################################################################
		# == CONJUGACOES ==
		#
		#        FN - Formas Nominais: infinitivo, gerundio e participio
		#        IP - Infinitivo Pessoal
		#
		#        PI - Presente do Indicativo
		#        II - Imperfeito do Indicativo
		#        EI - pErfeito do Indicativo
		#        MI - Mais-que-perfeito do Indicativo
		#        TI - fuTuro do preterito do Indicativo
		#        FI - Futuro do presente do Indicativo
		#
		#        PS - Presente do Subjuntivo
		#        IS - Imperfeito do Subjuntivo
		#        FS - Futuro do Subjuntivo
		#
		#        IA - Imperativo Afirmativo
		#        IN - Imperativo Negativo
		FN = u"(FN)[:](" + word + ")?[:](" + word + ")?[:](" + word + ")?"
		CONJ = u"(IP|PI|II|EI|MI|TI|FI|PS|IS|FS)[:]("+word+")?[:]("+word+")?[:]("+word+")?[:]("+word+")?[:]("+word+")?[:]("+word+")?"
		IAIN = u"(IA|IN)[:](" + word + ")?[:](" + word + ")?[:](" + word + ")?[:](" + word + ")?[:](" + word + ")?"

		##################################################################
		# == PARADIGMAS ==
		#
		# Um paradigma e seguido pelo infinitivo do verbo que serve como
		# paradigma, que por sua vez e seguido pelo sufixo (terminacao)
		#
		# Exemplo "paradigma:cantar:ar"
		#
		# == ABUNDANTES ==
		#
		# Alguns verbos aceitam uma segunda forma do participio
		# trocando-se "ado" na forma regular por "o".
		#
		# Exemplo: aceitado and aceito
		paradigma = "(abundante|paradigma)([:])(" + word + ")?[:](" + word + ")?"
		
		##################################################################
		# == SEGUIDORES ==
		#
		# Lista de verbos que seguem um paradigma
		follow = "(" + word + ")"

		##################################################################
		# == NEWLINE ==
		newline = "\r?\n|\r"

		return format([comments, FN, CONJ, IAIN, paradigma, follow, newline])

	def __nextLine(self):
		if self.hasNext():
			self.__next = unicode(self.__dbh.readline(), encoding="utf-8")
			if self.hasNext():
				self.__nextlineno += 1

	def next(self):
		self.__line, self.__lineno = self.__next, self.__nextlineno
		self.__nextLine()

		if self.hasNext():
			for i in self.__re:
				m = i.match(self.__line)
				if m != None:
					grps = m.groups()
					if len(grps) == 0:
						grps = ("newline",)	# Newline does not memorize
					return grps

			if not self.__errorHandler(self.__lineno, self.__line):
				raise TokenizerException(self)
		return None

	def line(self):
		return self.__line

	def close(self):
		self.__dbh.close()

	def hasNext(self):
		return self.__next != ""

	def lineno(self):
		return self.__lineno

def __main(argv):
	pass

if __name__ == "__main__":
	__main(sys.argv)
