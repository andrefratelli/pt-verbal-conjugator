#!/usr/bin/python

import sys
from parser import Parser
from silabas import DivisorSilabico

def tokenizerHandler(lineno, line):
	return False

def parserHandler():
	return False

class Conjugador:

	def __init__(self):
		parser = Parser(tokenizerHandler, parserHandler)
		self.__accessor = parser.parse(open("verbos/verbos"))
		self.__divisor = DivisorSilabico("verbos/")
		parser.close()

		for i in ["acariciar", "aceitar", "amiudar", "bramar", "jubilar", "encantar", "deduzir"]:
			print self.paradigma(i)

	def __syllabificateSuffix(self, infinitive):
		syllabas = self.__divisor.get(infinitive)

		while syllabas != []:
			sfx = ""
			for sil in syllabas:
				sfx += sil
			sfx = self.__accessor.suffix(sfx)

			if sfx != []: return sfx
			syllabas = syllabas[1:]
		return []

	def __deduceSuffix(self, infinitive):
		sfx = infinitive
		while sfx != "":
			ids = self.__accessor.suffix(sfx)
			if ids != []: return ids
			sfx = sfx[1:]
		return []

	def sufixo(self, infinitive):
		# Try to find the suffix through syllabfication
		# If we can't find it through syllabification, try cutting the word
		# If it fails, there's nothing more we can do
		syllabas = self.__syllabificateSuffix(infinitive)

		if syllabas == []:
			syllabas = self.__deduceSuffix(infinitive)

		return syllabas

	def paradigma(self, infinitive):

		# Try to find the infinitive in the database. In case of failure, deduce a paradigm
		paradigm = self.__accessor.follows(infinitive)

		if paradigm == None:
			paradigm = self.sufixo(infinitive)
			print ".",

			# :: TODO :: See below
			if len(paradigm) > 0: return self.__accessor.paradigm(paradigm[0])

			print ".",

			# :: TODO :: Uncomment this. Several paradigms are found. Which one to choose?
			#for i in self.sufixo(infinitive):
			#	print self.__accessor.paradigm(i),
			#print

		return paradigm

	def conjuga(self, infinitive):

		"""
		# :: TODO :: Melhorar algoritmo de procura.
		# :: TODO :: Um verbo registado como follow conjuga da mesma formar que o verbo que segue
		# :: TODO :: Um verbo registado como follow None usa a terminacao, mas a terminacao aqui esta incorrecta.
		# :: TODO :: Julgo que e necessario escancao silabica para encontrar a terminacao certa. Exemplo:
		#
		# terminar - terminacao encontrada (inar) nao esta correcta

		aux = infinitivo
		while aux != "":
			if aux in self.__rules['terminacoes'].keys():
				return self.__rules['ids'][self.__rules['terminacoes'][aux]]
			aux = aux[1::]
		return None
		"""
		pass

def __main(argv):
	Conjugador()

if __name__ == "__main__":
	__main(sys.argv)
