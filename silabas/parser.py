#!/usr/bin/python

from tokenizer import Tokenizer
from capsule import Capsule
import sys

class Parser:

	def __init__(self, tokenizerHandler, parserHandler):
		self.__tokenizer = None
		self.__tokenizerHandler = tokenizerHandler
		self.__parserHandler = parserHandler
		self.__ruleid = -1
		self.__parserules = {
			"#": self.__handleComment,
			"FN": self.__handleFN,
			"IP": self.__handleMultiPart,
			"PI": self.__handleMultiPart,
			"II": self.__handleMultiPart,
			"EI": self.__handleMultiPart,
			"MI": self.__handleMultiPart,
			"TI": self.__handleMultiPart,
			"FI": self.__handleMultiPart,
			"PS": self.__handleMultiPart,
			"IS": self.__handleMultiPart,
			"FS": self.__handleMultiPart,
			"IA": self.__handleIAIN,
			"IN": self.__handleIAIN,
			"paradigma": self.__handleParadigma,
			"newline": self.__handleNewLine,
			"word": self.__handleWord,
		}
		self.__rules = {
			'default': -1,		# The default paradigm
			'info': {},
			'paradigma': {},
			'terminacao': {},
			'follow': {},
			'followers': {},
			'radical': {},
			'FN': {},		# Formas Nominais: infinitivo, gerundio e participio
			'IP': {},		# Infinitivo Pessoal
			'PI': {},		# Presente do Indicativo
			'II': {},		# Imperfeito do Indicativo
			'EI': {},		# Perfeito do Indicativo
			'MI': {},		# Mais-que-perfeito do indicativo
			'TI': {},		# Futuro do preterito do indicativo
			'FI': {},		# Futuro do presente do indicativo
			'PS': {},		# Presente do subjuntivo
			'IS': {},		# Imperfeito do subjuntivo
			'FS': {},		# Futuro do subjuntivo
			'IA': {},		# Imperativo afirmativo
			'IN': {}		# Imperativo negativo
		}

	def __handleComment(self, token):
		pass

	def __handleFN(self, token):

		# Repeating this would override previous values
		assert self.__ruleid not in self.__rules['FN']

		self.__rules['FN'][self.__ruleid] = (token[1], token[2], token[3])

	def __handleMultiPart(self, token):

		# Repeating this would override previous values
		assert self.__ruleid not in self.__rules[token[0]]

		self.__rules[token[0]][self.__ruleid] = (token[1], token[2], token[3], token[4], token[5], token[6])

	def __handleIAIN(self, token):

		# Repeating this would override previous values
		assert self.__ruleid not in self.__rules[token[0]]

		self.__rules[token[0]][self.__ruleid] = (token[1], token[2], token[3], token[4], token[5])

	def __addFollower(self, id, follower):
		if id not in self.__rules["followers"]:
			self.__rules["followers"][id] = []
		self.__rules["followers"][id].append(follower)

	def __handleParadigma(self, token):
		self.__ruleid += 1

		paradigma = token[2]
		terminacao = token[3]
		radical = None

		if paradigma != None and terminacao != None:
			radical = paradigma[:len(paradigma) - len(terminacao)]

		self.__rules['info'][self.__ruleid] = {
			'paradigma': paradigma,
			'terminacao': terminacao,
			'radical': radical
		}

		if terminacao not in self.__rules['terminacao']:
			self.__rules['terminacao'][terminacao] = []

		if radical not in self.__rules['radical']:
			self.__rules['radical'][radical] = []

		if self.__ruleid not in self.__rules['terminacao'][terminacao]:
			self.__rules['terminacao'][terminacao].append(self.__ruleid)

		if self.__ruleid not in self.__rules['radical'][radical]:
			self.__rules['radical'][radical].append(self.__ruleid)

		# Repeating these would override previous values
		assert paradigma not in self.__rules['paradigma']
		assert paradigma not in self.__rules['follow']

		self.__rules['paradigma'][paradigma] = self.__ruleid
		self.__rules['follow'][paradigma] = self.__ruleid
		self.__addFollower(self.__ruleid, paradigma)

		# The last paradigm is the default paradigm
		self.__rules["default"] = self.__ruleid

	def __handleNewLine(self, token):
		pass

	def __handleWord(self, token):
		# Lone words are follow verbs
		if token[1] not in self.__rules['follow']:
			if self.__ruleid not in self.__rules['followers']:
				self.__rules['followers'][self.__ruleid] = []

			self.__rules['follow'][token[1]] = self.__ruleid
			self.__addFollower(self.__ruleid, token[1])

	def __decode(self, token):
		if token != None:

			# Follow verbs do not have a specific line start.
			# Also note that the word "paradigma" might incorrectly
			# show as a follow verb, which would create a conflict
			# with the rule key with the same name
			if len(token) < 2 and token[0] == "paradigma" or not token[0] in self.__parserules.keys():
				token = ("word",) + token

			self.__parserules[token[0]](token)

	def parse(self, dbh):
		tokenizer = Tokenizer(dbh, self.__tokenizerHandler)
		while tokenizer.hasNext():
			self.__decode(tokenizer.next())
		return Capsule(self.__rules)

	def close(self):
		if self.__tokenizer != None:
			self.__tokenizer.close()
			self.__ruleid = -1


def __main(argv):
	pass

if __name__ == "__main__":
	__main(sys.argv)

