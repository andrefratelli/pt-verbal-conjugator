#!/usr/bin/python

import sys
import urllib2
import htmllib
import formatter
import re
import time

__URL_SYLLABIFICATION = "http://www.portaldalinguaportuguesa.org/index.php?action=divisao&act=list&letter={0}&start={1}"
__URL_SYLLABIFICATION_INC = 100
__URL_SYLLABIFICATION_INTERVAL = 5 # In seconds

class HTMLParser(htmllib.HTMLParser):
	pass

class Formatter(formatter.AbstractFormatter):
	pass

class Writer(formatter.AbstractWriter):

	def __init__(self):
		self.__re = re.compile("verbo")
		self.__seguintes = re.compile("seguintes")
		self.__word = re.compile(u"[\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB]+")
		self.__verb = False
		self.__current = []
		self.__result = {}
		self.__hasnext = False
		self.__total = 0

		# The verbs that show at the end of the page are not terminated by a blank line
		# Example: acanteirar (http://www.portaldalinguaportuguesa.org/index.php?action=divisao&act=list&letter=a&start=2001)
		self.__handleIncorrectEnding = re.compile("[0-9]+ - [0-9]+ de ([0-9]+) palavras com")

	def __join(self):
		buf, parts = "", []
		for i in self.__current:
			buf += i
			parts.append(i)
		self.__result[buf] = parts

	def new_alignment(self, align): pass
	def new_font(self, font): pass
	def new_margin(self, margin, level): pass
	def new_spacing(self, spacing): pass
	def new_styles(self, styles): pass
	def send_line_break(self): pass
	def send_paragraph(self, blankline): pass
	def send_hor_rule(self, args, kw): pass

	def send_flowing_data(self, data):
		if self.__re.search(data) != None:
			self.__verb = True
		elif self.__handleIncorrectEnding.search(data):
			self.__verb = False
			self.__total = self.__handleIncorrectEnding.match(data).group(1)
		elif self.__verb:
			if data == " ":
				self.__verb = False
				self.__join()
				self.__current = []
			if self.__word.search(data):
				self.__current += [data.strip()]
		elif self.__seguintes.search(data):
			self.__hasnext = True

	def send_literal_data(self, data): pass
	def send_label_data(self, data): pass

	def verbs(self):
		return self.__result

	def hasnext(self):
		return self.__hasnext

	def reset(self):
		self.__hasnext = False
		self.__result = {}

	def total(self):
		return self.__total

def __write(output, verbs):
	for i in verbs:
		if len(verbs[i]) > 0:
			buf = ""
			for j in range(len(verbs[i])-1):
				buf += verbs[i][j] + "."
			buf += verbs[i][-1] + "\n"
			output.write(buf)


def __main(argv):
	writer = Writer()
	parser = HTMLParser(Formatter(writer))
	for letter in "defghijklmnopqrstuvwxyz":	# abc
		silabas = open("verbos/" + letter, "w")
		start = 0
		while writer.hasnext() or start == 0:
			print "Processing...",
			request = urllib2.urlopen(__URL_SYLLABIFICATION.format(letter, start))
			writer.reset()
			parser.feed(request.read())
			parser.close()
			print "[{0}] of [{1}] with letter [{2}]".format(start, writer.total(), letter)
			start += __URL_SYLLABIFICATION_INC
			__write(silabas, writer.verbs())

			time.sleep(__URL_SYLLABIFICATION_INTERVAL)
		silabas.write("\n")
		silabas.close()

if __name__ == "__main__":
	__main(sys.argv)

