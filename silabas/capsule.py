#!/usr/bin/python

import sys

class Capsule:

	def __init__(self, rules):
		self.__rules = rules
		self.__idsrules = {
			'info': True,
			'paradigma': False,
			'terminacao': False,
			'follow': False,
			'followers': True,
			'radical': False,
			'FN': True,
			'IP': True,
			'PI': True, 
			'II': True, 
			'EI': True, 
			'MI': True, 
			'TI': True, 
			'FI': True, 
			'PS': True,
			'IS': True,
			'FS': True, 
			'IA': True, 
			'IN': True 
		}

	def paradigm(self, id):
		if id in self.__rules["info"]:
			return self.__rules["info"][id]["paradigma"]

	def follows(self, infinitive):
		if infinitive in self.__rules["follow"]:
			return self.paradigm(self.__rules["follow"][infinitive])

	def suffix(self, suffix):
		if suffix in self.__rules["terminacao"]:
			return self.__rules["terminacao"][suffix]
		else: return []

	def __output(self):
		def output(rules, depth):
			for i in rules:
				print "\t" * depth,
				if type(rules[i]) == type({}):
					print i
					output(rules[i], depth + 1)
				else:
					print [i], [rules[i]]
		output(self.__rules, 0)

def __main(argv):
	pass

if __name__ == "__main__":
	__main(sys.argv)

