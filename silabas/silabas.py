#!/usr/bin/python

import sys
import os

class DivisorSilabico:

	def __init__(self, basedir):
		if basedir.endswith(os.sep):
			basedir = basedir
		else: basedir = basedir + os.sep

		self.__basedir = basedir
		self.__dictionaries = {}

		self.getDictionary("a")
		self.getDictionary("b")

	def __load(self, letter):
		self.__dictionaries[letter] = {}

		fh = open(self.__basedir + letter)
		word = fh.readline()
		while word != "":
			aux = word.strip()
			if aux != '':
				final = ""
				parts, aux = [], aux.split(".")
				for i in aux:
					final += i
					parts += i.split("-")
				self.__dictionaries[letter][final] = parts
			word = fh.readline()

	def getDictionary(self, letter):
		if letter not in self.__dictionaries:
			self.__load(letter)
		return self.__dictionaries[letter]

	def get(self, word):
		# TODO :: palavras acentuadas
		aux = self.getDictionary(word[0])
		if word in aux:
			return self.getDictionary(word[0])[word]
		else: return []

def __main(argv):
	divisor = DivisorSilabico("verbos")
	line = raw_input()
	while line != "":
		print divisor.get(line)
		line = raw_input()

if __name__ == "__main__":
	__main(sys.argv)
