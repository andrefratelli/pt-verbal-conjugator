#!/usr/bin/python

from curry import Curry

def mul(x,y):
	return x * y

foo = Curry(mul, 5)

print foo(3)
