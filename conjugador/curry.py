#!/usr/bin/python

import sys

class Curry:

	def __init__(self, callback, *args, **kwargs):
		self.__callback = callback
		self.__args = args[:]
		self.__kwargs = kwargs.copy()

	def __call__(self, *args, **kwargs):
		if kwargs and self.__kwargs:
			kw = self.__kwargs.copy()
			kw.update(kwargs)
		else:
			kw = kwargs or self.__kwargs
		return self.__callback(*(self.__args + args), **kw)

def __main(argv):
	pass

if __name__ == "__main__":
	__main(sys.argv)

