#!/usr/bin/python
# -*- coding: latin-1 -*-

import sys
import httplib
import time
import htmllib
from HTMLParser import HTMLParser
import re
import codecs
import pickle

__URL = "www.portaldalinguaportuguesa.org"
__BUNDLE_QUERY = "/index.php?lemmasel=exact&restrict=verbo&show=list&action=search&act=advanced&&start={0}"
__VERB_QUERY = "/index.php?action=lemma&lemma={0}"
__URL_INC = 150
__URL_MIN = 0
__URL_MAX = 14500 # 14500
__URL_INTERVAL = 1 # Seconds
__BUNDLE_COUNT = 0

def __encode(text):
	try:
		return unicode(text, encoding="latin-1")
	except UnicodeDecodeError: return text
	except TypeError: return text

class BundleParser(HTMLParser):

	def handle_starttag(self, tag, attrs):
		self.__initialize()

		if len(attrs) >= 1:
			info = attrs[0]
			if len(info) > 1:
				label, context = info
				if label == "label":
					self.__expecting = context
				elif label == "href":
					m = self.__queryRE.match(context)
					if m != None:
						self.__queries.append((self.__expecting, m.group(1)))

	def __initialize(self):
		aux = None
		try:
			aux = self.__queries
		except AttributeError:
			aux = self.__queries = []
			self.__expecting = ''
			self.__queryRE = re.compile("[?]action=lemma&lemma=([0-9]+)")
		return aux

	def next(self):
		aux = self.__initialize()
		self.__queries = []
		return aux

class VerbParser(HTMLParser):

	def handle_data(self, data):
		self.__initialize()

		if self.__state == 3: return

		try:
			data = unicode(data, encoding="latin-1")
			match = self.__re.match(data)
			if match != None:
				if self.__state == 2:
					if self.__transitions[self.__state].match(data) != None:
						self.__state += 1

					elif self.__ignore.match(data) == None:
						self.__process(data)
				elif self.__transitions[self.__state].match(data) != None:
					self.__state += 1
		except UnicodeDecodeError:
			print "UnicodeDecodeError: cannot decode \"{0}\"".format(data)

	def __process(self, data):
		match = self.__break.match(data)
		if match != None:
			self.__conjugations.append(match.groups())

	def next(self):
		aux = self.__conjugations
		self.__state = 0
		self.__conjugations = []
		return aux

	def __initialize(self):
		try:
			aux = self.__initialized
		except AttributeError:
			self.__initialized = False
			self.__re = re.compile(u"([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+)")
			self.__break = re.compile(
				u"([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+)(?:[ \t]*\(([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+)\)|[ \t]*[/][ \t]*([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+))?"
			)
			self.__ignore = re.compile(u"^rito|imperfeito|ndio|pio passado|coment\u00E1rios.*$")
			self.__state = 0
			self.__conjugations = []
			self.__transitions = {
				0: re.compile(u'mais-que-perfeito'),
				1: re.compile(u'perfeito'),
				2: re.compile(u'^.*:.*$'),
			}

def __processConjugation(infinitive, num, conjugations):
        result = {}

        # IP   : Presente do indicativo
        # IPI  : Pretérito imperfeito do indicativo
        # CP   : Presente do conjuntivo
        # IPMP : Pretérito mais-que-perfeito do indicativo
        # CPI  : Pretérito imperfeito do conjuntivo
        # CF   : Futuro do conjuntivo
        # IFI  : Futuro do presente (Futuro imperfeito do indicativo)
        # IFP  : Futuro perfeito do indicativo (condicional)
        # IP   : Infinitivo pessoal
        # G    : Gerúndio
        # PP   : Particípio passado
        # I    : Infinitivo

        result['IP'] = conjugations[:6]
        result['IPI'] = conjugations[6:12]
        result['IPP'] = conjugations[12:18]
        result['IPMP'] = conjugations[18:24]
        result['IFI'] = conjugations[24:30]
        result['IFP'] = conjugations[30:36]
        result['I'] = conjugations[36:37]
        result['CP'] = conjugations[37:43]
        result['CPI'] = conjugations[43:49]
        result['CF'] = conjugations[49:55]
        result['IA'] = conjugations[55:61]
        result['IN'] = conjugations[55:61]
        result['IP'] = conjugations[61:67]
        result['G'] = conjugations[67:68]
        result['PP'] = conjugations[68:72]

        return result

def __process(conn, data, num):
        global __BUNDLE_COUNT

	bundle = BundleParser()
	bundle.feed(data)
	cnt = 1
        result = {}
	for it in bundle.next():
		time.sleep(__URL_INTERVAL)
		conn.request("GET", __VERB_QUERY.format(it[1]))
		response = conn.getresponse()
		if response.status == 200:
			print num + cnt, "/", 14520, __encode(it[0]), "...",
			parser = VerbParser()
			parser.feed(response.read())

                        result[__BUNDLE_COUNT] = __processConjugation(it[0], num, parser.next())
                        __BUNDLE_COUNT += 1

                        print "done"
			cnt += 1
		else: print "Request failed: {0} {1}".format(response.status, response.reason)
        return result


def __request(conn, start):
        result = {}
	print "Retrieving new page...",
	conn.request("GET", __BUNDLE_QUERY.format(start))
	response = conn.getresponse()
	if response.status == 200:
		print "done"
		result = dict(result, **__process(conn, response.read(), start))
                return result
	else: print "Request failed: {0} {1}".format(response.status, response.reason)

def __main(argv):
        result = {}
	for it in range(__URL_MIN, __URL_MAX, __URL_INC):
		conn = httplib.HTTPConnection(__URL)
		result = dict(result, **__request(conn, it))
		conn.close()
		time.sleep(__URL_INTERVAL)

        fh = open("stuff", "w")
        pickle.dump(result, fh)
        fh.close()

if __name__ == "__main__":
	__main(sys.argv)

