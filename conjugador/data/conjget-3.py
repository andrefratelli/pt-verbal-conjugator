#!/usr/bin/python
import sys
import httplib
import time
import htmllib
from HTMLParser import HTMLParser
import re
import codecs

__URL = "www.portaldalinguaportuguesa.org"
__BUNDLE_QUERY = "/index.php?lemmasel=exact&restrict=verbo&show=list&action=search&act=advanced&&start={0}"
__VERB_QUERY = "/index.php?action=lemma&lemma={0}"
__URL_INC = 150
__URL_MIN = 0
__URL_MAX = 14500 # 14500
__URL_INTERVAL = 1 # Seconds

__RULES = {}
__NORULE = []
__TERMINATIONS = []
__ABUNDANTES = {}

class BundleParser(HTMLParser):

	def handle_starttag(self, tag, attrs):
		self.__initialize()

		if len(attrs) >= 1:
			info = attrs[0]
			if len(info) > 1:
				label, context = info
				if label == "label":
					self.__expecting = context
				elif label == "href":
					m = self.__queryRE.match(context)
					if m != None:
						self.__queries.append((self.__expecting, m.group(1)))

	def __initialize(self):
		aux = None
		try:
			aux = self.__queries
		except AttributeError:
			aux = self.__queries = []
			self.__expecting = ''
			self.__queryRE = re.compile("[?]action=lemma&lemma=([0-9]+)")
		return aux

	def next(self):
		aux = self.__initialize()
		self.__queries = []
		return aux

class VerbParser(HTMLParser):

	def handle_data(self, data):
		self.__initialize()

		if self.__state == 3: return

		try:
			data = unicode(data, encoding="latin-1")
			match = self.__re.match(data)
			if match != None:
				if self.__state == 2:
					if self.__transitions[self.__state].match(data) != None:
						self.__state += 1

					elif self.__ignore.match(data) == None:
						self.__process(data)
				elif self.__transitions[self.__state].match(data) != None:
					self.__state += 1
		except UnicodeDecodeError:
			print "UnicodeDecodeError: cannot decode \"{0}\"".format(data)

	def __process(self, data):
		match = self.__break.match(data)
		if match != None:
			self.__conjugations.append(match.groups())

	def next(self):
		aux = self.__conjugations
		self.__state = 0
		self.__conjugations = []
		return aux

	def __initialize(self):
		try:
			aux = self.__initialized
		except AttributeError:
			self.__initialized = False
			self.__re = re.compile(u"([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+)")
			self.__break = re.compile(
				u"([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+)(?:[ \t]*\(([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+)\)|[ \t]*[/][ \t]*([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+))?"
			)
			self.__ignore = re.compile(u"^rito|imperfeito|ndio|[-]|pio passado|coment\u00E1rios.*$")
			self.__state = 0
			self.__conjugations = []
			self.__transitions = {
				0: re.compile(u'mais-que-perfeito'),
				1: re.compile(u'perfeito'),
				2: re.compile(u'^.*:.*$'),
			}

def __encode(text):
	try:
		return unicode(text, encoding="latin-1")
	except UnicodeDecodeError: return text
	except TypeError: return text

def __normalize(conj):
	ret = []
	for it in conj:
		ret.append(it[0])
	return ret

def __extract(conj):
	ret = []
	for it in conj:
		if it[1] == None:
			ret.append(it[0])
		else: ret.append(it[1])
	return ret

def __processConjugation(infinitive, num, conjugations):
	if len(conjugations) != 71:
		__NORULE.append(infinitive)
		print "wrong combinations", len(conjugations)
		return False

	conj = {}
	conj['PI'] = __normalize(conjugations[:6])
	conj['II'] = __normalize(conjugations[6:12])

	conj['EI'] = conjugations[12:18]
	abundante = conj['EI'][3][2]
	conj['EI'] = __normalize(conj['EI'])
	if abundante != None:
		conj['EI'][3], abundante = abundante, conj['EI'][3]
	else: abundante = ''

	conj['MI'] = __normalize(conjugations[18:24])
	conj['TI'] = __normalize(conjugations[24:30])
	conj['FI'] = __normalize(conjugations[30:36])

	conj['FN'] = __normalize(conjugations[36:37])

	conj['PS'] = __normalize(conjugations[37:43])
	conj['IS'] = __normalize(conjugations[43:49])
	conj['FS'] = __normalize(conjugations[49:55])

	conj['IA'] = conjugations[55:60]
	conj['IN'] = __extract(conj['IA'])
	conj['IA'] = __normalize(conj['IA'])

	conj['IP'] = __normalize(conjugations[60:66])

	conj['FN'] += __normalize(conjugations[66:68])

	__RULES[infinitive] = {
		'abundante': abundante,
		'paradigm': {
			'FN': conj['FN'],
			'IP': conj['IP'],
			'PI': conj['PI'],
			'II': conj['II'],
			'EI': conj['EI'],
			'MI': conj['MI'],
			'TI': conj['TI'],
			'FI': conj['FI'],
			'PS': conj['PS'],
			'IS': conj['IS'],
			'FS': conj['FS'],
			'IA': conj['IA'],
			'IN': conj['IN'],
		},
	}

	return True

def __process(conn, data, num):
	bundle = BundleParser()
	bundle.feed(data)
	cnt = 1
	for it in bundle.next():
		time.sleep(__URL_INTERVAL)
		conn.request("GET", __VERB_QUERY.format(it[1]))
		response = conn.getresponse()
		if response.status == 200:
			print num + cnt, "/", 14520, __encode(it[0]), "...",
			parser = VerbParser()
			parser.feed(response.read())
			if __processConjugation(it[0], num, parser.next()):
				print "done"
			cnt += 1
		else: print "Request failed: {0} {1}".format(response.status, response.reason)

def __request(conn, start):
	print "Retrieving new page...",
	conn.request("GET", __BUNDLE_QUERY.format(start))
	response = conn.getresponse()
	if response.status == 200:
		print "done"
		__process(conn, response.read(), start)
	else: print "Request failed: {0} {1}".format(response.status, response.reason)


def __main(argv):
	for it in range(__URL_MIN, __URL_MAX, __URL_INC):
		conn = httplib.HTTPConnection(__URL)
		__request(conn, it)
		conn.close()
		time.sleep(__URL_INTERVAL)

	# Write to file!
	fh = codecs.open("verbs", "w", "latin-1")
	for it in __RULES:
		aux = __encode(it) + __encode(":") + __encode(__RULES[it]['abundante'])
		fh.write(aux)
		fh.write("\n")
		for jt in __RULES[it]['paradigm']:

			aux = __encode(jt) + __encode(":")
			fh.write(aux)

			for kt in __RULES[it]['paradigm'][jt]:
				aux = __encode(kt) + __encode(":")
				fh.write(aux)

			fh.write("\n")
		fh.write("\n")
	fh.close()

	fh = codecs.open("norule", "w", "latin-1")
	for it in __NORULE:
		aux = __encode(it)

		fh.write(aux)
		fh.write("\n")
	fh.close()


if __name__ == "__main__":
	__main(sys.argv)
