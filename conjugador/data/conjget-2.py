#!/usr/bin/python

import sys
import httplib
import time
import htmllib
from HTMLParser import HTMLParser
import re
import codecs

class BundleParser(HTMLParser):

	def handle_starttag(self, tag, attrs):
		self.__initialize()

		if len(attrs) >= 1:
			info = attrs[0]
			if len(info) > 1:
				label, context = info
				if label == "label":
					self.__expecting = context
				elif label == "href":
					m = self.__queryRE.match(context)
					if m != None:
						self.__queries.append((self.__expecting, m.group(1)))

	def __initialize(self):
		aux = None
		try:
			aux = self.__queries
		except AttributeError:
			aux = self.__queries = []
			self.__expecting = ''
			self.__queryRE = re.compile("[?]action=lemma&lemma=([0-9]+)")
		return aux

	def next(self):
		aux = self.__initialize()
		self.__queries = []
		return aux

class VerbParser(HTMLParser):

	def handle_data(self, data):
		self.__initialize()

		if self.__state == 3: return

		try:
			data = unicode(data, encoding="latin-1")
			match = self.__re.match(data)
			if match != None:
				if self.__state == 2:
					if self.__transitions[self.__state].match(data) != None:
						self.__state += 1

					elif self.__ignore.match(data) == None:
						self.__process(data)
				elif self.__transitions[self.__state].match(data) != None:
					self.__state += 1
		except UnicodeDecodeError:
			print "UnicodeDecodeError: cannot decode \"{0}\"".format(data)

	def __process(self, data):
		match = self.__break.match(data)
		if match != None:
			self.__conjugations.append(match.groups())

	def next(self):
		aux = self.__conjugations
		self.__state = 0
		self.__conjugations = []
		return aux

	def __initialize(self):
		try:
			aux = self.__initialized
		except AttributeError:
			self.__initialized = False
			self.__re = re.compile(u"([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+)")
			self.__break = re.compile(
				u"([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+)(?:[ \t]*\(([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+)\)|[ \t]*[/][ \t]*([\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+))?"
			)
			self.__ignore = re.compile(u"^rito|imperfeito|ndio|[-]|pio passado|coment\u00E1rios.*$")
			self.__state = 0
			self.__conjugations = []
			self.__transitions = {
				0: re.compile(u'mais-que-perfeito'),
				1: re.compile(u'perfeito'),
				2: re.compile(u'^.*:.*$'),
			}

__URL = "www.portaldalinguaportuguesa.org"
__BUNDLE_QUERY = "/index.php?lemmasel=exact&restrict=verbo&show=list&action=search&act=advanced&&start={0}"
__VERB_QUERY = "/index.php?action=lemma&lemma={0}"
__URL_INC = 150
__URL_MIN = 0
__URL_MAX = 145500
__URL_INTERVAL = 3 # Seconds

__RULES = {}
__NORULE = []
__TERMINATIONS = []
__ABUNDANTES = {}

def __breakVerb(infinitive, verb):
	aux = infinitive
	for it in ['FN','IP','PI','II','EI','MI','TI','FI','PS','IS','FS','IA','IN']:
		for jt in verb['paradigm'][it].keys():
			xis = verb['paradigm'][it][jt]
			try:
				aux = unicode(aux, encoding="latin-1")
			except TypeError: pass

			while re.compile(xis).match(aux + xis[len(aux):]) == None and aux != '':
				aux = aux[:-1]
				try:
					aux = unicode(aux, encoding="latin-1")
				except TypeError: pass
				

	for it in ['FN','IP','PI','II','EI','MI','TI','FI','PS','IS','FS','IA','IN']:
		for jt in verb['paradigm'][it].keys():
			verb['paradigm'][it][jt] = verb['paradigm'][it][jt][len(aux):]

	verb['abundante'] = verb['abundante'][len(aux):]

	return (aux, infinitive[len(aux):])

def __findParadigm(verb):
	match = None
	equals = False
	for it in __RULES:
		equals = True
		match = __RULES[it]
		for jt in __RULES[it]['paradigm']:
			if jt != 'parent' and jt != 'followers':
				for kt in __RULES[it]['paradigm'][jt]:
					if __RULES[it]['paradigm'][jt][kt] != verb['paradigm'][jt][kt]:
						equals = False
						break
				if not equals: break
		if equals: break

	if equals == True: return match
	else: return None

def __findParentParadigm(paradigm):
	topmax = 0
	parent = None
	if paradigm != None:
		for it in __RULES:
			count = 0
			candidate = False
			for jt in __RULES[it]['paradigm']:
				if jt != 'parent' and jt != 'followers':
					for kt in __RULES[it]['paradigm'][jt]:
						if __RULES[it]['paradigm'][jt][kt] == paradigm['paradigm'][jt][kt]:
							count += 1
						else: candidate = True
			if count > topmax and candidate == True:
				topmax = count
				parent = __RULES[it]
	return parent

def __cleanParadigm(paradigm):
	parent = paradigm['paradigm']['parent']
	for it in paradigm['paradigm']:
		if it != 'parent' and it != 'followers':
			for jt in paradigm['paradigm'][it]:
				if paradigm['paradigm'][it][jt] == parent['paradigm'][it][jt]:
					paradigm['paradigm'][it][jt] = None

def __finally(infinitive, verb):
	verb['radical'], verb['termination'] = __breakVerb(infinitive, verb)

	global __ABUNDANTES
	__ABUNDANTES[infinitive] = verb['abundante']

	paradigm = __findParadigm(verb)

	if paradigm != None and paradigm['paradigm']['parent'] == None:
		parent = __findParentParadigm(paradigm)

		if parent != None:
			paradigm['paradigm']['parent'] = parent
			__cleanParadigm(paradigm)

	print "paradigm",
	if paradigm == None:
		__RULES[infinitive] = verb
		print "(new)",
	else:
		paradigm['paradigm']['followers'].append(verb['radical'])
		print "(" + paradigm['radical'], paradigm['termination'] + ")",
	print "...",

def __normalize(conj):
	ret = []
	for it in conj:
		ret.append(it[0])
	return ret

def __extract(conj):
	ret = []
	for it in conj:
		if it[1] == None:
			ret.append(it[0])
		else: ret.append(it[1])
	return ret

def __hashify(conj):
	res, count = {}, 0
	for i in ['1S','2S','3S','1P','2P','3P']:
		res[i] = conj[count]
		count += 1
	return res

def __hashify_2(conj):
	res, count = {}, 0
	for i in ['INFINITIVO', 'GERUNDIO', 'PARTICIPIO']:
		conj[count]
		res[i] = conj[count]
		count += 1
	return res

def __hashify_3(conj):
	res, count = {}, 0
	for i in ['2S','3S','1P','2P','3P']:
		res[i] = conj[count]
		count += 1
	return res

def __processConjugation(infinitive, num, conjugations):
	ret = True
	if len(conjugations) != 71:
		__NORULE.append(infinitive)
		print "wrong combinations", len(conjugations)
		return False

	conj = {}
	conj['PI'] = __normalize(conjugations[:6])
	conj['II'] = __normalize(conjugations[6:12])

	conj['EI'] = conjugations[12:18]
	abundante = conj['EI'][3][2]
	conj['EI'] = __normalize(conj['EI'])
	if abundante != None:
		conj['EI'][3], abundante = abundante, conj['EI'][3]
	else: abundante = ''

	conj['MI'] = __normalize(conjugations[18:24])
	conj['TI'] = __normalize(conjugations[24:30])
	conj['FI'] = __normalize(conjugations[30:36])

	conj['FN'] = __normalize(conjugations[36:37])

	conj['PS'] = __normalize(conjugations[37:43])
	conj['IS'] = __normalize(conjugations[43:49])
	conj['FS'] = __normalize(conjugations[49:55])

	conj['IA'] = conjugations[55:60]
	conj['IN'] = __extract(conj['IA'])
	conj['IA'] = __normalize(conj['IA'])

	conj['IP'] = __normalize(conjugations[60:66])

	conj['FN'] += __normalize(conjugations[66:68])

	if ret:
		__finally(infinitive, {
			'radical': '',
			'termination': '',
			'abundante': abundante,
			'paradigm': {
				'parent': None,
				'FN': __hashify_2(conj['FN']),
				'IP': __hashify(conj['IP']),
				'PI': __hashify(conj['PI']),
				'II': __hashify(conj['II']),
				'EI': __hashify(conj['EI']),
				'MI': __hashify(conj['MI']),
				'TI': __hashify(conj['TI']),
				'FI': __hashify(conj['FI']),
				'PS': __hashify(conj['PS']),
				'IS': __hashify(conj['IS']),
				'FS': __hashify(conj['FS']),
				'IA': __hashify_3(conj['IA']),
				'IN': __hashify_3(conj['IN']),
				'followers': [],
			},
		})
	return ret

def __process(conn, data, num):
	bundle = BundleParser()
	bundle.feed(data)
	cnt = 1
	for it in bundle.next():
		time.sleep(__URL_INTERVAL)
		conn.request("GET", __VERB_QUERY.format(it[1]))
		response = conn.getresponse()
		if response.status == 200:
			print num + cnt, "/", 14520, __encode(it[0]), "...",
			parser = VerbParser()
			parser.feed(response.read())
			if __processConjugation(it[0], num, parser.next()):
				print "done"
			cnt += 1
		else: print "Request failed: {0} {1}".format(response.status, response.reason)
		

def __request(conn, start):
	print "Retrieving new page...",
	conn.request("GET", __BUNDLE_QUERY.format(start))
	response = conn.getresponse()
	if response.status == 200:
		print "done"
		__process(conn, response.read(), start)
	else: print "Request failed: {0} {1}".format(response.status, response.reason)

def __encode(text):
	try:
		return unicode(text, encoding="latin-1")
	except UnicodeDecodeError: return text
	except TypeError: return text

def __getPessoas(tempo):
	if tempo == 'FN':
		return ['INFINITIVO', 'GERUNDIO', 'PARTICIPIO']
	elif tempo == 'IA' or tempo == 'IN':
		return ['2S','3S','1P','2P','3P']
	elif tempo in ['IP','PI','II','EI','MI','TI','FI','PS','IS','FS']:
		return ['1S','2S','3S','1P','2P','3P']
	else: raise UnknownTemporalException(tempo)

__PREVIOUS = []

def __getConj(paradigm, tempo, pessoa):
	while paradigm != None:
		if paradigm['paradigm'][tempo][pessoa] != None:
			return paradigm['paradigm'][tempo][pessoa]

		paradigm = paradigm['paradigm']['parent']
	return None

def __cleanFromPrevious(paradigm):
	global __PREVIOUS

	unmatchable = {
		'FN': {},
		'IP': {},
		'PI': {}, 
		'II': {}, 
		'EI': {}, 
		'MI': {}, 
		'TI': {}, 
		'FI': {},
		'PS': {}, 
		'IS': {}, 
		'FS': {}, 
		'IA': {}, 
		'IN': {}, 
	}

	for it in __PREVIOUS:
		for jt in it['paradigm']:
			if jt != 'parent' and jt != 'followers':
				for kt in it['paradigm'][jt]:
					try:
						aux = unmatchable[jt][kt]
					except KeyError:
						if paradigm['paradigm'][jt][kt] == it['paradigm'][jt][kt]:
							paradigm['paradigm'][jt][kt] = None
						elif it['paradigm'][jt][kt] != None: unmatchable[jt][kt] = True

def outputParadigms(paradigm, fh):
	try:
		aux = paradigm['processed']
		return False
	except KeyError: pass

	if paradigm['paradigm']['parent'] != None:
		if not outputParadigms(paradigm['paradigm']['parent'], fh):
			out=__encode("restaurar:")+__encode(paradigm['paradigm']['parent']['radical'])+__encode(paradigm['paradigm']['parent']['termination'])

	__cleanFromPrevious(paradigm)

	out = __encode(paradigm['radical']) + __encode(paradigm['termination'])
	out = __encode("paradigma:") + out + __encode(":") + __encode(paradigm['termination']) + __encode("\n")
	fh.write(out)

	for kt in paradigm['paradigm']:
		if kt != 'parent' and kt != 'followers':
			doit = False
			for lt in paradigm['paradigm'][kt]:
				if paradigm['paradigm'][kt][lt] != None:
					doit = True
					break
			if doit:
				fh.write("{0}".format(kt))
				for lt in __getPessoas(kt):
					out = ":"
					if paradigm['paradigm'][kt][lt] != None:
						out += paradigm['paradigm'][kt][lt]
					out = __encode(out)
					fh.write(out)
				fh.write('\n')
	fh.write('\n')

	if len(paradigm['paradigm']['followers']) > 0:
		out = __encode("# Followers for ") + __encode(paradigm['radical']) + __encode(paradigm['termination']) + "\n"
		fh.write(out)
		for lt in paradigm['paradigm']['followers']:
			fh.write(__encode(lt) + __encode(paradigm['termination']) + "\n")
		fh.write("\n")

	if paradigm['abundante'] != '':
		out = __encode("abundante:") + __encode(paradigm['radical']) + __encode(paradigm['termination']) + __encode(":") + __encode(paradigm['abundante'])
		fh.write(out + "\n")

		for lt in paradigm['paradigm']['followers']:
			global __ABUNDANTES
			abundante = __encode(__ABUNDANTES[lt + paradigm['termination']])

			fh.write(__encode("abundante:") + __encode(lt + paradigm['termination']) + __encode(":") + abundante + "\n")
		fh.write("\n")

	paradigm['processed'] = True

	global __PREVIOUS
	__PREVIOUS.insert(0, paradigm)

	return True

def __main(argv):
	for it in range(__URL_MIN, __URL_MAX, __URL_INC):
		conn = httplib.HTTPConnection(__URL)
		__request(conn, it)
		conn.close()
		time.sleep(__URL_INTERVAL)

	# Write to file!
	fh = codecs.open("verbs", "w", "latin-1")
	for it in __RULES:
		outputParadigms(__RULES[it], fh)

	out = __encode("paradigma::\n")
	fh.write(out)
	for it in __NORULE:
		fh.write(__encode(it) + "\n")
	fh.write("\n")
	fh.close()

if __name__ == "__main__":
	__main(sys.argv)

