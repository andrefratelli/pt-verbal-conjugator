#!/usr/bin/python

import sys

class Token:

	def __init__(self, token, type):
		self.__token = token
		self.__type = type

	def token(self): return self.__token
	def type(self): return self.__type

def __main(argv):
	pass

if __name__ == "__main__":
	__main(sys.argv)
