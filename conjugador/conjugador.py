#!/usr/bin/python

from tokenizer import Tokenizer
from parser import Parser
import sys
import re
from curry import Curry

def __encode(text):
	try:
		text = unicode(text, "latin-1")
	except UnicodeDecodeError: pass
	except TypeError: pass

	return text

class ConjugueException(Exception):
	pass

class RestoreException(ConjugueException):

	def __init__(self, verb, parser):
		self.__verb = verb
		self.__lineno = parser.lineno()

	def __str__(self):
		return __encode("Restoring an unknown verb: ") + __encode(self.verb())

	def verb(self): return self.__verb
	def lineno(self): return self.__lineno

class RepeatedParadigmException(ConjugueException):

	def __init__(self, infinitive, parser):
		self.__infinitive = __encode(infinitive)
		self.__lineno = __encode(str(parser.lineno()))

	def __str__(self):
		return __encode("Input is invalid duo to repeated paradigm \"")+self.verb() + __encode("\"at line ") + self.lineno()

	def verb(self): return self.__infinitive
	def lineno(self): return self.__lineno

class AbundanteException(ConjugueException):

	def __init__(self, infinitive, parser):
		self.__infinitive = infinitive
		self.__lineno = parser.lineno()

	def __str__(self):
		return __encode("Abundante not found: ") + __encode(self.verb())

	def verb(self): return self.__infinitive
	def lineno(self): return self.__lineno

class NotVerbException(ConjugueException):

	def __init__(self, infinitive):
		self.__infinitive = infinitive

	def __str__(self):
		return "Not a verb: " + self.verb()

	def verb(self): return self.__infinitive

class UnknownTemporalException(ConjugueException):

	def __init__(self, tempo):
		self.__tempo = tempo

	def __str__(self):
		return __encode("Unknown temporal form: ") + __encode(self.temporal())

	def temporal(self): return self.__tempo

class Conjugador:

	def __init__(self, dbp):
		tokenizer = Tokenizer(
			{
				'PARADIGMA': re.compile(u"paradigma:"),
				'ABUNDANTE': re.compile(u"abundante:"),
				'RESTAURAR': re.compile(u"restaurar:"),
				'SEP': re.compile(u":"),
				'FN': re.compile(u"FN:"),
				'IP': re.compile(u"IP:"),
				'PI': re.compile(u"PI:"),
				'II': re.compile(u"II:"),
				'EI': re.compile(u"EI:"),
				'MI': re.compile(u"MI:"),
				'TI': re.compile(u"TI:"),
				'FI': re.compile(u"FI:"),
				'PS': re.compile(u"PS:"),
				'IS': re.compile(u"IS:"),
				'FS': re.compile(u"FS:"),
				'IA': re.compile(u"IA:"),
				'IN': re.compile(u"IN:"),
				'WORD': re.compile(u"[\u0061-\u007A\u00E0-\u00E3\u00E6-\u00EA\u00EC-\u00EE\u00F2-\u00F5\u00F9-\u00FB\-]+"),
				'COMMENT': re.compile(u"#.*"),
				'WHITESPACE': re.compile(u"[ \t]+"),
				'NEWLINE': re.compile(u"\r?\n|\r"),
			},
			[
				'PARADIGMA','ABUNDANTE','RESTAURAR','SEP',
				'FN','IP','PI','II','EI','MI','TI','FI','PS','IS','FS','IA','IN',
				'WORD','COMMENT','WHITESPACE','NEWLINE'
			],
			self.__tokenizerSyntaxHandler
		)

		parser = Parser(
			tokenizer,
			'S_START',
			{
				'S_START': {
					'PARADIGMA': (None, 'S_PARADIGMA'),
					'ABUNDANTE': (None, 'S_ABUNDANTE'),
					'RESTAURAR': (None, 'S_RESTAURAR'),
					'FN': (Curry(self.__activateConjugation, 'FN'), 'S_FN_INFINITIVE'),
					'IP': (Curry(self.__activateConjugation, 'IP'), 'S_CONJUGATION_S_1ST'),
					'PI': (Curry(self.__activateConjugation, 'PI'), 'S_CONJUGATION_S_1ST'),
					'II': (Curry(self.__activateConjugation, 'II'), 'S_CONJUGATION_S_1ST'),
					'EI': (Curry(self.__activateConjugation, 'EI'), 'S_CONJUGATION_S_1ST'),
					'MI': (Curry(self.__activateConjugation, 'MI'), 'S_CONJUGATION_S_1ST'),
					'TI': (Curry(self.__activateConjugation, 'TI'), 'S_CONJUGATION_S_1ST'),
					'FI': (Curry(self.__activateConjugation, 'FI'), 'S_CONJUGATION_S_1ST'),
					'PS': (Curry(self.__activateConjugation, 'PS'), 'S_CONJUGATION_S_1ST'),
					'IS': (Curry(self.__activateConjugation, 'IS'), 'S_CONJUGATION_S_1ST'),
					'FS': (Curry(self.__activateConjugation, 'FS'), 'S_CONJUGATION_S_1ST'),
					'IA': (Curry(self.__activateConjugation, 'IA'), 'S_IAIN_S_2ND'),
					'IN': (Curry(self.__activateConjugation, 'IN'), 'S_IAIN_S_2ND'),
					'WORD': (self.__handleFollower, 'S_START'),
					'COMMENT': (None, 'S_START'),
					'WHITESPACE': (None, 'S_START'),
					'NEWLINE': (None, 'S_START'),
				},
				'S_FALLBACK': {
					'WHITESPACE': (None, 'S_FALLBACK'),
					'COMMENT': (None, 'S_FALLBACK'),
					'NEWLINE': (None, 'S_START'),
				},
				'S_SEP': {
					'SEP': (None, '.'),
				},
				'S_PARADIGMA': {
					'WORD': (self.__handleParadigma, 'S_SEP', 'S_TERMINATION'),
					'SEP': (self.__handleDefaultParadigma, 'S_FALLBACK'),
				},
				'S_TERMINATION': {
					'WORD': (self.__handleTermination, 'S_FALLBACK'),
				},
				'S_ABUNDANTE': {
					'WORD': (self.__handleAbundante, 'S_SEP', 'S_ABUNDANTE_2'),
				},
				'S_ABUNDANTE_2': {
					'WORD': (self.__handleAbundante, 'S_FALLBACK'),
				},
				'S_RESTAURAR': {
					'WORD': (self.__handleRestaurar, 'S_FALLBACK'),
				},
				'S_FN_INFINITIVE': {
					'WORD': (Curry(self.__handleConjugation, 'INFINITIVO'), 'S_SEP', 'S_FN_GERUND'),
					'SEP': (None, 'S_FN_GERUND'),
				},
				'S_FN_GERUND': {
					'WORD': (Curry(self.__handleConjugation, 'GERUNDIO'), 'S_SEP', 'S_FN_PARTICIPLE'),
					'SEP': (None, 'S_FN_PARTICIPLE'),
				},
				'S_FN_PARTICIPLE': {
					'WORD': (Curry(self.__handleConjugation, 'PARTICIPIO'), 'S_FALLBACK'),
					'WHITESPACE': (None, 'S_START'),
					'COMMENT': (None, 'S_START'),
					'NEWLINE': (None, 'S_START'),
				},
				'S_CONJUGATION_S_1ST': {
					'WORD': (Curry(self.__handleConjugation, '1S'), 'S_SEP', 'S_CONJUGATION_S_2ND'),
					'SEP': (None, 'S_CONJUGATION_S_2ND'),
				},
				'S_CONJUGATION_S_2ND': {
					'WORD': (Curry(self.__handleConjugation, '2S'), 'S_SEP', 'S_CONJUGATION_S_3RD'),
					'SEP': (None, 'S_CONJUGATION_S_3RD'),
				},
				'S_CONJUGATION_S_3RD': {
					'WORD': (Curry(self.__handleConjugation, '3S'), 'S_SEP', 'S_CONJUGATION_P_1ST'),
					'SEP': (None, 'S_CONJUGATION_P_1ST'),
				},
				'S_CONJUGATION_P_1ST': {
					'WORD': (Curry(self.__handleConjugation, '1P'), 'S_SEP', 'S_CONJUGATION_P_2ND'),
					'SEP': (None, 'S_CONJUGATION_P_2ND'),
				},
				'S_CONJUGATION_P_2ND': {
					'WORD': (Curry(self.__handleConjugation, '2P'), 'S_SEP', 'S_CONJUGATION_P_3RD'),
					'SEP': (None, 'S_CONJUGATION_P_3RD'),
				},
				'S_CONJUGATION_P_3RD': {
					'WORD': (Curry(self.__handleConjugation, '3P'), 'S_FALLBACK'),
					'WHITESPACE': (None, 'S_START'),
					'COMMENT': (None, 'S_START'),
					'NEWLINE': (None, 'S_START'),
				},
				'S_IAIN_S_2ND': {
					'WORD': (Curry(self.__handleConjugation, '2S'), 'S_SEP', 'S_IAIN_S_3RD'),
					'SEP': (None, 'S_IAIN_S_3RD'),
				},
				'S_IAIN_S_3RD': {
					'WORD': (Curry(self.__handleConjugation, '3S'), 'S_SEP', 'S_IAIN_P_1ST'),
					'SEP': (None, 'S_IAIN_P_1ST'),
				},
				'S_IAIN_P_1ST': {
					'WORD': (Curry(self.__handleConjugation, '1P'), 'S_SEP', 'S_IAIN_P_2ND'),
					'SEP': (None, 'S_IAIN_P_2ND'),
				},
				'S_IAIN_P_2ND': {
					'WORD': (Curry(self.__handleConjugation, '2P'), 'S_SEP', 'S_IAIN_P_3RD'),
					'SEP': (None, 'S_IAIN_P_3RD'),
				},
				'S_IAIN_P_3RD': {
					'WORD': (Curry(self.__handleConjugation, '3P'), 'S_FALLBACK'),
					'WHITESPACE': (None, 'S_START'),
					'COMMENT': (None, 'S_START'),
					'NEWLINE': (None, 'S_START'),
				},
			}
		)

		self.__verbs = {}
		self.__paradigms = {}
		self.__activeInfinitive = ''
		self.__activeAbundante = ''
		self.__active = self.__makeParadigm(None)

		self.__verbs[self.__activeInfinitive] = {
			'radical': '',
			'termination': '',
			'abundante': '',
			'paradigm': self.__active,
		}

		self.__activeConjugation = ''

		dbh = open(dbp, "r")
		parser.parse(dbh)
		dbh.close()

	def __tokenizerSyntaxHandler(self, tokenizer, text):
		return False

	def __parserSyntaxHandler(self, parser, text):
		return False

	def __activateConjugation(self, conj, parser, token, transition):
		self.__activeConjugation = conj

	def __normalize(self, infinitive, termination):
		return infinitive, termination, infinitive[:len(infinitive) - len(termination)]

	def __handleParadigma(self, parser, token, transition):
		infinitive = token.token()
		if infinitive not in self.__verbs:
			self.__paradigms[self.__activeInfinitive] = self.__active

			self.__activeInfinitive = infinitive
			self.__verbs[self.__activeInfinitive] = {}
			self.__verbs[self.__activeInfinitive]['paradigm'] = self.__active = self.__makeParadigm(self.__active)
		else: raise RepeatedParadigmException(infinitive, parser)

	def __makeParadigm(self, parent):
		return {
			'parent': parent,
			'FN': {},
			'IP': {},
			'PI': {},
			'II': {},
			'EI': {},
			'MI': {},
			'TI': {},
			'FI': {},
			'PS': {},
			'IS': {},
			'FS': {},
			'IA': {},
			'IN': {},
		}

	def __handleTermination(self, parser, token, transition):
		infinitive, termination, radical = self.__normalize(self.__activeInfinitive, token.token())

		self.__verbs[infinitive]['termination'] = termination
		self.__verbs[infinitive]['radical'] = radical

	def __handleDefaultParadigma(self, parser, token, transition):
		self.__paradigms[self.__activeInfinitive] = self.__active

		self.__activeInfinitive = ''
		self.__active = self.__paradigms[self.__activeInfinitive]

	def __handleFollower(self, parser, token, transition):
		infinitive, termination, radical = self.__normalize(token.token(), self.__verbs[self.__activeInfinitive]['termination'])

		self.__verbs[infinitive] = {
			'abundante': '',
			'radical': radical,
			'termination': termination,
			'paradigm': self.__active,
		}

	def __handleRestaurar(self, parser, token, transition):
		infinitive = token.token()
		if infinitive in self.__paradigms:
			self.__active = self.__paradigms[infinitive]
		else: raise RestoreException(infinitive, parser)

	def __handleAbundante(self, parser, token, transition):
		if self.__activeAbundante == '':
			if token.token() in self.__verbs:
				self.__activeAbundante = token.token()
			else: raise AbundanteException(token.token(), parser)
		else:
			self.__verbs[self.__activeAbundante]['abundante'] = token.token()
			self.__activeAbundante = ''

	def __handleConjugation(self, conj, parser, token, transition):
		termination = token.token()
		self.__active[self.__activeConjugation][conj] = termination

	def __conjuga(self, paradigm):
		complete = True
		conjugation = {}
		for tempo in self.getTempos():
			conjugation[tempo] = {}
			for sp in self.getPessoas(tempo):
				if sp in paradigm[tempo]:
					conjugation[tempo][sp] = paradigm[tempo][sp]
				else: complete = False

		if not complete and paradigm['parent'] != None:
			parent = self.__conjuga(paradigm['parent'])
			for it in conjugation:
				if conjugation[it] == {}:
					conjugation[it] = parent[it]
				else:
					for jt in self.getPessoas(it):
						try:
							if conjugation[it][jt] == '':
								conjugation[it][jt] = parent[it][jt]
						except KeyError: conjugation[it][jt] = parent[it][jt]
		return conjugation

	def conjuga(self, infinitive, tempos):
		if infinitive in self.__verbs:
			conjugation = self.__conjuga(self.__verbs[infinitive]['paradigm'])
			radical = self.__verbs[infinitive]['radical']
			for it in conjugation:
				print "== {0} ==".format(it)
				for jt in self.getPessoas(it):
					out = u"[{0}]{1}".format(radical, conjugation[it][jt])
					if it == 'EI' and jt == '1P':
						out += u"/{0}".format(self.__verbs[infinitive]['abundante'])
					print jt, out

		else: raise NotVerbException(infinitive)

	def getTempos(self):
		return ['FN','IP','PI','II','EI','MI','TI','FI','PS','IS','FS','IA','IN']

	def getPessoas(self, tempo):
		if tempo == 'FN':
			return ['INFINITIVO', 'GERUNDIO', 'PARTICIPIO']
		elif tempo == 'IA' or tempo == 'IN':
			return ['2S','3S','1P','2P','3P']
		elif tempo in ['IP','PI','II','EI','MI','TI','FI','PS','IS','FS']:
			return ['1S','2S','3S','1P','2P','3P']
		else: raise UnknownTemporalException(tempo)

def __main(argv):
	Conjugador("data/verbs").conjuga("ab-rogar", ['IA', 'FN', 'II'])

if __name__ == "__main__":
	__main(sys.argv)

