#!/usr/bin/python

import sys
from tokenizer import Tokenizer
import re

class ParserException(Exception):
	pass

class UnknownTransitionException(ParserException):

	def __init__(self, parser, through, lineno):
		self.__parser = parser
		self.__state = parser.state()
		self.__through = through
		self.__lineno = lineno

	def __str__(self):
		return "Unknown transition state from \"{0}\" through \"{1}\" in grammar at line {2}".format(self.state(), self.through(), self.lineno())

	def parser(self): return self.__parser
	def state(self): return self.__state
	def through(self): return self.__through
	def lineno(self): return self.__lineno

class UnknowStartSymbolException(ParserException):

	def __init__(self, parser, symbol):
		self.__parser = parser
		self.__symbol = symbol

	def __str__(self):
		return "Unknown start symbol \"{0}\" in grammar".format(self.symbol())

	def parser(self): return self.__parser
	def symbol(self): return self.__symbol

class InvalidTransitionFormat(ParserException):

	def __init__(self, parser, transition):
		self.__parser = parser
		self.__transition = transition

	def __str__(self):
		return "Invalid transition {0} format".format(self.transition())

	def parser(self): return self.__parser
	def transition(self): return self.__transition

class InvalidStackOperation(ParserException):

	def __init__(self, parser, transition):
		self.__parser = parser
		self.__transition = transition
		self.__state = parser.state()

	def __str__(self):
		return "Poped from an empty stack when in state \"{0}\" transiting through {1}".format(self.state(), self.transition())

	def parser(self): return self.__parser
	def transition(self): return self.__transition
	def state(self): return self.__state

class Parser:

	def __init__(self, tokenizer, start, grammar, popSymbol='.'):
		self.__tokenizer = tokenizer
		self.__state = start
		self.__grammar = grammar
		self.__stack = []
		self.__popSymbol = popSymbol

		if self.__state not in self.__grammar:
			raise UnknowStartSymbolException(self, start)

	def __normalizeTransition(self, transition):
		if len(transition) == 2: return transition + ('',)
		elif len(transition) == 3: return transition
		else: raise InvalidTransitionFormat(self, transition)

	def __getTransition(self, transition, stackAction):
		state = ''
		if transition == self.__popSymbol:
			if len(self.__stack) > 0:
				state = self.__stack.pop()
			else: raise InvalidStackOperation(self, transition)
		else: state = transition

		if stackAction != '':
			self.__stack.append(stackAction)

		return state

	def parse(self, fh):
		def __handler(tokenizer, token, lineno):
			if token.type() in self.__grammar[self.__state]:
				action, transition, stackAction = self.__normalizeTransition(self.__grammar[self.__state][token.type()])

				if action != None: action(self, token, transition)

				self.__state = self.__getTransition(transition, stackAction)

			else: raise UnknownTransitionException(self, token.type(), lineno)

		self.__tokenizer.parse(fh, __handler)

	def state(self):
		return self.__state

	def lineno(self):
		return self.__tokenizer.lineno()

def __main(argv):
	pass

if __name__ == "__main__":
	__main(sys.argv)

