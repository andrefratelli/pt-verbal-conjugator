#!/usr/bin/python

import sys
import re
from token import Token

class TokenizerException(Exception):

	def __init__(self, tokenizer, text, lineno):
		self.__tokenizer = tokenizer
		self.__text = text
		self.__lineno = lineno

	def __str__(self):
		return "Unexpected token {0} at line {1}.".format([self.text()], self.lineno())

	def tokenizer(self): return self.__tokenizer
	def text(self): return self.__text
	def lineno(self): return self.__lineno

class Tokenizer:

	def __init__(self, symbols, order, syntaxHandler):
		self.__symbols = symbols
		self.__order = order
		self.__syntaxHandler = syntaxHandler

	def __match(self, text, lineno):
		for it in self.__order:
			aux = text

			# Sometimes the tokenizer (re?) returns a different encoding.
			# We can't always encode (TypeError: encoding unicode is not possible)
			# so we catch the exception and encode only when needed
			try:
				text = unicode(text, encoding="latin-1")
			except TypeError: pass
			except UnicodeDecodeError: pass

			m = self.__symbols[it].match(text)
			if m != None:
				return Token(m.group(0), it)

		if not self.__syntaxHandler(self, text):
			raise TokenizerException(self, text, lineno)

	def parse(self, fh, handler):
		lineno = 1
		line = unicode(fh.readline(), encoding="utf-8")
		while line != "":
			token = self.__match(line, lineno)
			if token != None:
				handler(self, token, lineno)

				# It seems that len calculates the wrong size if
				# we do not encode or something... This is already
				# done in __match, but if we don't do it again here
				# it doesn't work
				try:
					line = unicode(line, encoding="latin-1")
				except TypeError: pass
				except UnicodeDecodeError: pass

				line = line[len(token.token()):]

			else: line = unicode("", encoding="utf-8")

			if line == u"":
				line = fh.readline()
				lineno += 1

def __main(argv):
	pass

if __name__ == "__main__":
	__main(sys.argv)
